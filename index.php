<?php
// Paramètres à renseigner avant utilisation
$url = "https://monsite.demo.sugarcrm.eu/rest/v11";
$login = "xxx";
$passwd = "xxx";
$maxNum = 10000;
//----------------------------------------------------------
include 'vendor/autoload.php';

include 'ApiSugar.php';

// Initialisation Twig
$loader = new Twig\Loader\FilesystemLoader('templates');
$twig = new Twig\Environment($loader);
$template = $twig->load('base.twig.html');
$paramsRender = [];
//----------------------------------------------------------
// Récupération du token d'accès
$api = new ApiSugar($url, $login, $passwd);
$token = $api->getToken();
$paramsRender = array_merge($paramsRender,['token' => $token]);
//----------------------------------------------------------
// Récupérer la liste des contacts (nom, prénom, adresse, email) dont le prénom contient « a » ou le nom contient « b »
$params = [
  "filter" => [
    [
      '$or' => [
        ["first_name" => ['$contains' => 'a']],
        ["last_name" => ['$contains' => 'b']]
      ],
    ],
  ],
  "max_num" => $maxNum
];
$contacts = $api->request('POST', '/Contacts/filter', $params);
$paramsRender = array_merge($paramsRender,['contacts' => count($contacts) > 0 ? $contacts : []]);
//----------------------------------------------------------
// Récupérer les tickets non clos de ce contact
if (count($contacts) > 0) {
  $premierContact = $contacts[0];
  $paramsRender = array_merge($paramsRender,['premierContact' => $premierContact]);
  $params = [
    "filter" => [
          [
            "status" => [ '$not_equals' => "Closed" ]
          ]     
    ],
    "deleted"=> false,
    "max_num" => $maxNum
  ];  
  $tickets = $api->request('GET', '/Contact/' . $premierContact->id . '/Cases', $params);
  //var_dump(current($tickets));
  $paramsRender = array_merge($paramsRender,['tickets' => count($tickets) > 0 ? $tickets : []]);
  //----------------------------------------------------------
  // Crée un nouveau ticket
  $params = [
     "name" => "Test Add New Ticket",     
     "account_type" => "Customer",
     "description" => "Description of new Ticket",
     "priority" => "P1",
     "status" => "New",
     "account_id" => $premierContact->account_id,
     "assigned_to"=> $premierContact->id
  ];
  $newTicket = $api->request('POST', '/Cases/' , $params);
  $paramsRender = array_merge($paramsRender,['newticket' => $newTicket]);   
}
//----------------------------------------------------------s
echo $template->render(
  $paramsRender
);
