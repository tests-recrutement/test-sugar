<?php

/**
 * ApiSugar : sert a faire des appels a l'API REST de SUGAR
 */
class ApiSugar
{

  /**
   * token : le jeton d'authentification
   */
  private $token;

  /**
   * username : le login
   */
  private $username;

  /**
   * password : le mot de passe
   */
  private $password;

  /**
   * client : l'identifiant client
   */
  private $client;

  /**
   * baseUrl : l'url de base pour appeler l'API REST SUGAR
   */
  private $baseUrl;

  /**
   * __construct
   *
   * @param  string $baseUrl : l'url ou se situe l'API REST SUGAR
   * @param  string $username : le login
   * @param  string $password : le mot de passe
   * @param  string $client : le client (défaut "sugar")
   * @return void
   */
  function __construct(string $baseUrl, string $username, string $password, string $client = "sugar")
  {
    $this->username = $username;
    $this->password = $password;
    $this->client = $client;
    $this->baseUrl = $baseUrl;
    $this->token = $this->generateToken();
  }

  /**
   * generateToken : Génère un nouveau token
   *
   * @return string : le token que l'on vient de générer
   */
  private function generateToken(): string
  {
    $params = [
      "grant_type" => "password",
      "client_secret" => "",
      "client_id" => $this->client,
      "username" => $this->username,
      "password" => $this->password,
      "platform" => "base"
    ];

    $ch = curl_init($this->baseUrl . "/oauth2/token");
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      "Content-Type: application/json"
    ));
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));

    $tokenResponse = curl_exec($ch);
    $token = json_decode($tokenResponse, true);

    return $token['access_token'];
  }


  /**
   * getToken : retourne le token courant
   *
   * @return string : le token courant
   */
  public function getToken(): string
  {
    return $this->token;
  }


  /**
   * request
   *
   * @param  string $method
   * @param  string $url : 
   * @param  string $parameters : les paramètres à passer à l'API
   * @return renvoie les records ou la réponse si pas de records
   */
  public function request(string $method, string $url, array $parameters = []) 
  {
    ob_start();
    $ch = curl_init();

    if ($method == 'GET') {
      $data = http_build_query($parameters);
      $url = $url . "?" . $data;
    }
    curl_setopt($ch, CURLOPT_URL, $this->baseUrl . $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    if ($method == 'POST') {
      curl_setopt($ch, CURLOPT_POST, 1);
      $post = json_encode($parameters);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch,
      CURLOPT_HTTPHEADER,
      [
        "Content-Type: application/json",
        "oauth-token: {$this->token}"
      ]
    );
    $result = curl_exec($ch);
    curl_close($ch);

    $result = explode("\r\n\r\n", $result, 2);
    $response = json_decode($result[1]);
    ob_end_flush();
    return $response && property_exists($response, 'records') ? $response->records : $response;
  }
}
