# Test technique - APPELS API SUGAR

Test technique appel API de SUGAR CRM, 

## Objectif
L'objectif est de réaliser différents tests d'appels à l'API de SUGAR CRM.
Note : j'ai essayé de ne pas noyer le code dans un gros framework, j'ai seulement utilisé twig comme moteur de rendu pour réparer un minimum la vue.

## Tâches effectuées
- [ ] Lecture exercice
- [ ] Tests API SUGAR CRM avec Postman
- [ ] Codage fonctionnalités
- [ ] Tests fonctionnels partiels (manque de temps) : comparaison avec retours attendus, corrections 
- [ ] Amélioration du rendu visuel 
- [ ] Ecriture doc
- [ ] Mise sur Gitlab

## Installation

- [ ] Récupération du code source
- [ ] Installer twig
```
composer install
```
- [ ] Mettre les infos de connexion a l'API (fichier index.php)
```
// Paramètres à renseigner avant utilisation
$url = "https://monsite.demo.sugarcrm.eu/rest/v11";
$login = "xxx";
$passwd = "xxx";
```
- [ ] Lancement du serveur de base de php pour tester (utilisez un port libre) :
```
php -S localhost:8888
```

## Fichiers modifiés

- [ ] index.php : les appels à l'API
- [ ] ApiSugar.php : classe servant à appeler l'API de Sugar
- [ ] templates\*.twig.html : les différents templates
- [ ] composer.json : le fichier pour installer twig

## A améliorer

- [ ] Terminer les tests fonctionnels que je n'ai pas eu le temps de faire
- [ ] Rajouter des tests unitaires (pour la classe ApiSugar notamment) 
- [ ] ...

## Incompréhension / Interprétation

- [ ] Pas de notion de rendu, mocks ou autre : j'ai rendu les données pour qu'elles soient visible par un humain

## Environnement de développement

Serveur web utilisé : celui de base
```
php -S localhost:8888
```

## Temps passé

- [ ] 30mn : découverte de sugarCRM
- [ ] 1h30 : découverte de l'API de sugarCRM, lecture documentation, ...
- [ ] 30mn : initialisation du code (installation de twig, squelette html, ...)
- [ ] 2h00 : code PHP appel API / class ApiSugar
- [ ] 30mn : CSS 
- [ ] 20mn : Tests fonctionnels (partiels) + retours 
- [ ] 20mn : Documentation - README.md
- [ ] 10mn : GIT